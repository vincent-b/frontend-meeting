# Frontend meeting examples


Install NodeJS modules likewise:

```bash
	$ npm install
```


Start it with:

```bash
	$ npm start
```


Vincent Bruijn

vincent-bruijn@g-star.com
