var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Frontend meeting' });
});

router.get('/fm1', function(req, res, next) {
	res.render('fm1', { title: 'Frontend meeting 1' });
});

module.exports = router;
